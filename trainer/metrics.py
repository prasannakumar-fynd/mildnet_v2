from tensorflow.keras import backend as K
import tensorflow as tf

_EPSILON = K.epsilon()


def hinge_loss_fn(batch_size):
    def hinge_loss(y_true, y_pred):
        y_pred = K.clip(y_pred, _EPSILON, 1.0 - _EPSILON)
        loss = tf.convert_to_tensor(0, dtype=tf.float32)
        g = tf.constant(1.0, shape=[1], dtype=tf.float32)

        for i in range(0, batch_size, 3):
            try:
                q_embd = y_pred[i + 0]
                p_embd = y_pred[i + 1]
                n_embd = y_pred[i + 2]
                Dqp = K.sqrt(K.sum((q_embd - p_embd) ** 2))
                Dqn = K.sqrt(K.sum((q_embd - n_embd) ** 2))
                loss = (loss + g + Dqp - Dqn)
                
            except (tf.errors.InvalidArgumentError, ValueError):
                break

        loss = loss / (batch_size / 3) 
        zero = tf.constant(0.0, shape=[1], dtype=tf.float32)
        return tf.maximum(loss, zero)

    return hinge_loss


def contrastive_loss_fn(batch_size):
    def contrastive_loss(y_true, y_pred):
        def _contrastive_loss(y1, D):
            g = tf.constant(1.0, shape=[1], dtype=tf.float32)
            return K.mean(y1 * K.square(D) + (g - y1) * K.square(K.maximum(g - D, 0)))

        y_pred = K.clip(y_pred, _EPSILON, 1.0 - _EPSILON)
        loss = tf.convert_to_tensor(0, dtype=tf.float32)
        g = tf.constant(1.0, shape=[1], dtype=tf.float32)
        h = tf.constant(0.0, shape=[1], dtype=tf.float32)

        for i in range(0, batch_size, 3):
            try:
                q_embedding = y_pred[i + 0]
                p_embedding = y_pred[i + 1]
                n_embedding = y_pred[i + 2]
                Dqp = K.sqrt(K.sum((q_embedding - p_embedding) ** 2))
                Dqn = K.sqrt(K.sum((q_embedding - n_embedding) ** 2))
                Lqp = _contrastive_loss(g, Dqp)
                Lqn = _contrastive_loss(h, Dqn)
                loss = loss + Lqp + Lqn
                
            except (tf.errors.InvalidArgumentError, ValueError):
                break

        loss = loss / ((batch_size * 2) / 3)
        zero = tf.constant(0.0, shape=[1], dtype=tf.float32)
        return tf.maximum(loss, zero)

    return contrastive_loss


def accuracy_fn(batch_size):
    def accuracy(y_true, y_pred):
        y_pred = K.clip(y_pred, _EPSILON, 1.0 - _EPSILON)
        acc = 0

        for i in range(0, batch_size, 3):
            try:
                q_embedding = y_pred[i + 0]
                p_embedding = y_pred[i + 1]
                n_embedding = y_pred[i + 2]
                Dqp = K.sqrt(K.sum((q_embedding - p_embedding) ** 2))
                Dqn = K.sqrt(K.sum((q_embedding - n_embedding) ** 2))
                acc += tf.cond(Dqn > Dqp, lambda: 1, lambda: 0)
                
            except (tf.errors.InvalidArgumentError, ValueError):
                break

        acc = tf.cast(acc, tf.float32)
        return (acc * 100) / (batch_size / 3)

    return accuracy


loss_functions = {'hinge': hinge_loss_fn, 'contrastive': contrastive_loss_fn}
