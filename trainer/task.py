import logging
# import tensorflow.logging as tf_logging
import os
from .utils import print_trainable_counts
from .models import models
from .metrics import accuracy_fn, loss_functions
from .datagen import ImageDataGenerator
from .checkpointers import HyperdashCallback
from hyperdash import Experiment
from tensorflow.train import MomentumOptimizer, RMSPropOptimizer
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from tensorflow.contrib.tpu import keras_to_tpu_model, TPUDistributionStrategy
from tensorflow.contrib.cluster_resolver import TPUClusterResolver
from argparse import ArgumentParser


def main(job_dir, model_name, optimizer, batch_size, loss, train_epochs, data_path, train_csv, val_csv,
         embedding_size, input_shape, lr=0.01, num_workers=1, num_batch_samples=None, weights_path=None, tpu_worker=None, hyperdash_key=None):
    
    logging.getLogger().setLevel(logging.INFO)
    # tf_logging.set_verbosity(tf_logging.INFO)

    if model_name not in models.keys():
        raise ValueError("Invalid argument entered, expected `{}`".format(model.keys()))

    if optimizer not in {'mo', 'rms'}:
        raise ValueError("Invalid argument entered, expected `mo` or `rms`")

    if loss not in {'hinge', 'contrastive'}:
        raise ValueError("Invalid argument entered, expected `hinge` or `contrastive`")

    if weights_path and not os.path.exists(weights_path):
        raise ValueError("Invalid weights_path")

    logging.info("Building Model: {} with embedding_size = {}".format(model_name, embedding_size))

    # model_summary 
    model = models.get(model_name)(embedding_size=embedding_size,
                                  input_shape=input_shape)
    # input_shape = [int(v) for v in model.input[0].shape[:2]]
    # input_shape = [int(v) for v in model.input[0].shape[:2]]
    trainable_count, non_trainable_count = print_trainable_counts(model)
    logging.info('trainable params count : {}'.format(trainable_count))
    logging.info('non trainable params count : {}'.format(non_trainable_count))

    # Setting up HyperDash
    def get_api_key():
        return hyperdash_key

    if hyperdash_key:
        logging.info("Setting up HyperDash")
        exp = Experiment(model_name, get_api_key, capture_io=True)
        exp.param("model_name", model_name)
        exp.param("data_path", data_path)
        exp.param("batch_size", batch_size)
        exp.param("train_epochs", train_epochs)
        exp.param("optimizer", optimizer)
        exp.param("lr", lr)

        if weights_path:
            exp.param("weights_path", weights_path)

        exp.param("loss", loss)
        exp.param("train_csv", train_csv)
        exp.param("val_csv", val_csv)
        exp.param("trainable_count", trainable_count)
        exp.param("non_trainable_count", non_trainable_count)

    # train sequences generator
    train_gen = ImageDataGenerator(data_path=data_path,
                                   triplets_csv=train_csv,
                                   target_size=input_shape[:2],
                                   batch_size=batch_size,
                                   num_batch_samples=num_batch_samples,
                                   embedding_size=embedding_size,
                                   shuffle=True,
                                   augment=True,
                                   rotation_range=30,
                                   zoom_range=0.2,
                                   shear_range=0.2,
                                   vertical_flip=False,
                                   horizontal_flip=True,
                                   height_shift_range=0.1,
                                   width_shift_range=0.1)

    # val sequences generator
    val_gen = ImageDataGenerator(data_path=data_path,
                                 triplets_csv=val_csv,
                                 target_size=input_shape,
                                 batch_size=batch_size,
                                 embedding_size=embedding_size)

    # model files dir
    job_dir = model_name + "_" + loss + "_" + job_dir
    if not os.path.exists(job_dir):
        os.mkdir(job_dir)

    # load pre-trained / checkpointed weights
    if weights_path:
        model.load_weights(weights_path)
        logging.info("Loaded model weights from {}".format(weights_path))

    # defining the optimizer, 
    opt_params = dict(loss=loss_functions.get(loss)(batch_size * 3),
                      metrics=[accuracy_fn(batch_size * 3)])
    opt_params['optimizer'] = MomentumOptimizer(lr, momentum=0.9, use_nesterov=True) \
        if optimizer == 'mo' else RMSPropOptimizer(lr)
    model.compile(**opt_params)

    # model_checkpoint callback
    weights_save_path = os.path.join(job_dir, 'weights')
    if not os.path.exists(weights_save_path):
        os.mkdir(weights_save_path)

    ckptr_path = os.path.join(weights_save_path, "epoch-{epoch:02d}-loss-{val_loss:.2f}.h5")
    model_ckptr = ModelCheckpoint(ckptr_path, save_best_only=True, save_weights_only=True, monitor="val_loss",
                                  verbose=1)

    # tensorboard callback
    logs_path = os.path.join(job_dir, 'logs')
    if not os.path.exists(logs_path):
        os.mkdir(logs_path)

    tensor_board = TensorBoard(log_dir=logs_path, histogram_freq=0, write_graph=True, write_images=True)

    # csv_logger callback
    csv_logger = CSVLogger(os.path.join(job_dir, 'training.log'))
    callbacks_list = [model_ckptr, tensor_board, csv_logger]
    
    if hyperdash_key:
        callbacks_list.append(HyperdashCallback(exp))

    if tpu_worker:
        model = keras_to_tpu_model(model, strategy=TPUDistributionStrategy(TPUClusterResolver(tpu_worker)))

    try:
        model.fit_generator(train_gen,
                            validation_data=val_gen,
                            epochs=train_epochs,
                            callbacks=callbacks_list,
                            max_queue_size=16,
                            workers=num_workers)

    except KeyboardInterrupt:
        intr_ckptr_path = os.path.join(weights_save_path, "interrupted_weights.h5")
        model.save_weights(intr_ckptr_path)
        logging.info("Model training interrupted, saved weights at {}".format(intr_ckptr_path))


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        '--job_dir',
        help="local path to write checkpoints and export models",
        required=True,
        type=str)

    parser.add_argument(
        '--data_path',
        help='local paths to training data, should contain images folder',
        required=True,
        type=str)

    parser.add_argument(
        '--optimizer',
        help='Optimizer',
        required=True,
        type=str)

    parser.add_argument(
        '--model_name',
        help='model name',
        required=True,
        type=str)

    parser.add_argument(
        '--weights_path',
        help='location of pre-trained weights path',
        default=None,
        type=str)

    parser.add_argument(
        '--loss',
        help='loss function',
        required=True,
        type=str)

    parser.add_argument(
        '--train_csv',
        help='path to train csv',
        default='train_triplets.csv',
        type=str)

    parser.add_argument(
        '--val_csv',
        help='path to val csv',
        default='val_triplets.csv',
        type=str)

    parser.add_argument(
        '--batch_size',
        help='batch size',
        type=int,
        default=16)

    parser.add_argument(
        '--num_batch_samples',
        help='number of batches to train per epoch',
        type=int)
    
    parser.add_argument(
        '--embedding_size',
        help='size of the embedding / feature vector ',
        type=int,
        default=1024)
    
    parser.add_argument(
        '--input_shape',
        help='shape of the input image(h, w, c)',
        type=tuple,
        default=(300, 300, 3))

    parser.add_argument(
        '--tpu_worker',
        help='tpu_worker if tpu is to be used',
        default=False,
        type=bool)

    parser.add_argument(
        '--train_epochs',
        help='number of epochs to train',
        default=6,
        type=int)

    parser.add_argument(
        '--num_workers',
        help='creates processes on cpu for faster batch loading',
        default=1,
        type=int)

    parser.add_argument(
        '--lr',
        help='learning rate',
        default=0.001,
        type=float)

    parser.add_argument(
        '--hyperdash_key',
        help='Hyperdash key',
        default=None,
        type=str)

    main(**parser.parse_args().__dict__)
